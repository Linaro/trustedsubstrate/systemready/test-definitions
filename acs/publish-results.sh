#!/bin/sh

set -x

SQUAD_URL=$1
SQUAD_TOKEN=$2

# Create a single test and metadata
curl -k \
    --header "Auth-Token: $SQUAD_TOKEN" \
    --form tests='{"acs/acs": "pass"}' \
    --form attachment="@/mnt/acs_results/sct_results/Overall/Summary.log" \
    --form attachment="@/mnt/acs_results/sct_results/Overall/Summary.ekl" \
    --form attachment="@/mnt/acs_results/linux_tools/dt-validate.log" \
    ${SQUAD_URL}
