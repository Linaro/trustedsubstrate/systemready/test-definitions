# Test definitions for ACS testing

For now, this repository only contains a small script that will run after ACS test suite
with the purpose of sending resulting log files back to SQUAD.

**NOTE:** LAVA test jobs running this definition will have a SQUAD token as paramater,
so please make sure to **not** run it publicly.
